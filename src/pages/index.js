import React from "react";
import Layout from "../templates/layout/layout"
// import 'bootstrap/dist/css/bootstrap.min.css';
import SEO from "../templates/seo";
import { graphql } from "gatsby";

import Profile from '../components/profile/profile';

const IndexPage = ({data}) => (
  <Layout>
    <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
    <Profile
      name="Jacob"
      title="Front End Web Developer"
      dob="28/02/1988"
      className="profile"
    />
        <p>I have launched Websites for small businesses and large enterprises such as Honda’s Landing page, Blackmagic Design Landing page and Legacy  Product pages for the likes of URSA Mini Pro, Davinci Resolve, ATEM Television Studio Pro and Cintel Scanner to name a few.</p>
        <p>I enjoy learning, applying new technology and constantly being challenged. I don't accept "That's how we have always done it" as an answer. I thrive on being challenged, learning as much as I can about new technology and how to apply it to new projects. I have a lot of experience working independently but thrive in a team environment where I can bounce ideas and theories off like minded people.</p>

        {data.allMarkdownRemark.edges.map(post => (
          <div key={post.node.id}>
            <div className="job-post-heading">
                  {post.node.frontmatter.title}
            </div>
            <div dangerouslySetInnerHTML={{ __html: post.node.html }} />
          </div>
        ))}
  </Layout>
)

export const pageQuery = graphql`
  query indexQuery {
    allMarkdownRemark(limit: 10) {
      edges {
        node {
          id
          html
          frontmatter {
            title
            path
          }
        }
      }
    }
  }
`
export default IndexPage