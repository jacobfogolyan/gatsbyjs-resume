import React from 'react';
import propTypes from "prop-types";
import './profile.scss'
import Image from './image'

const Profile = props => {
    return (
    <div className="row pb-3">
        <div className="col-7">
            <div className="row profile py-3">
                <div className="col-12 col-sm-3 col-md-4">
                    <Image style={{ borderRadius: '10px', maxWidth: "20px" }} />
                </div>
                <div className="col-12 col-sm-7 align-middle d-flex">
                    <ul className="align-self-center">
                        <li><strong>{props.name}</strong></li>
                        <li>{ props.title }</li>
                        <li>{ props.dob }</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    )
}

Profile.propTypes = {
    name: propTypes.string,
    title: propTypes.string,
    dob: propTypes.string
}

export default Profile;