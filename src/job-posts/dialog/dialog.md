---
path: "/dialog"
date: "2017-11-07"
endDate: ""
title: "Dialog IT"
---

Using web development tools and Content Management Systems (MySource Matrix) to deliver effective web content solutions for the Northern Territory Government. I was required to go on site and work closely with the IT managers and web teams to aid their content/development process to meet strict deadlines. Developing strong and professional relationships within such a small tightly knit community was essential to the success of this position.

##### Technology Used

**HTML, CSS, Javascript, Jquery, MySource Matrix.**
