---
path: "/leo-burnett"
date: "2017-11-07"
title: "Leo Burnett"
---

An Agile workplace working with producers, designers to achieve pixel perfect banner animations and EDM’s. Some of my achievements also include developing Honda’s Corporate Landing Page, Content Migration in AEM for Honda Foundation and detailed bug testing. I look for every opportunity to take one something new and to go above and beyond my role.

##### Technology Used

**Node.js, Gulp, SASS, HTML5, CSS3, Javascript, GreenSock, AEM, Google DoubleClick**
