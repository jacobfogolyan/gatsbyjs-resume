---
path: "/big-red-group"
date: "2017-11-07"
startDate: "2017-11-07"
endDate: ""
title: "Big Red Group"
---

This project involved creating Dynamic Animated Banners that were promotional advertisements for upcoming sales. These banners were automatically populated by JSON feed that was generated from an external system. Using javascript/GreenSock we pulled the information from the JSON Feed and manipulated/animated the content.

Once the information was populated into the page it would then automatically resize all DOM elements and spacing so it would still meet the style guide requirements.

##### Technology Used

**HTML5, CSS3, Javascript, Javascript/Greensock, Jquery**
