---
path: "/power-water"
date: "2017-11-07"
endDate: ""
title: "Power and Water Corporation"
---

Power and Water are the only Utility in the Northern Territory. It was my responsibility to coordinate media releases, layout changes and provide training to different Departments within Power and Water to manage their particular areas of their intranet project. This involved training documentation and small training groups of 5 to 8 people.

##### Technology Used

**HTML, CSS, Javascript, Jquery, Squiz Matrix.**
