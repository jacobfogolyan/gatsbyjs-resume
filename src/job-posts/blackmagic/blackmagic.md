---
path: "/blackmagic"
date: "2017-11-07"
title: "Blackmagic Design"
logo: "./logo.svg"
---

With over 100,000 views each week, Blackmagic most notably designs and manufactures Cinema Camera technology. Each Product has its own website and it was my job to work closely with designers to achieve pixel perfect mobile first designs within Angular using HTML/CSS/Javascript.

Being a Senior in our team has given me the exposure not only to Front End Technologies but interpersonal skills to train people and work with Translators on multilingual websites responsible for 11 different languages.

##### Technology Used

**Node.js, Gulp, SASS, HTML5, CSS3, Javascript, GreenSock, Jquery, bootstrap.**
