import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import '../styles/styles.scss';

const Header = ({ siteTitle, description }) => (
  <header>
    <h1 style={{ margin: 0 }}>
        <div className="container">
          <div className="row">
            <div className="col-12 col-md-9 col-xl-8 py-3 bd-content">
              <Link className="link" to="/">
                { siteTitle }
                { description }
              </Link>
            </div>
          </div>
        </div>
    </h1>
  </header>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
  description: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
  description: ``,
}

export default Header
