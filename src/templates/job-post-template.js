import React from 'react';
import { graphql } from 'gatsby';

export default function Template({
  data,
}) {
  const { markdownRemark: post } = data;

  return (
    <div className="job-post">
      <h2>{ post.frontmatter.title }</h2>
      <h3>{ post.frontmatter.logo }</h3>
      <div dangerouslySetInnerHTML={{__html: post.html }} />
    </div>
  )
}

export const postQuery = graphql`
  query BlogPostByPath($path: String!) {
    markdownRemark(frontmatter: { path: { eq: $path } }) {
      html
      frontmatter {
        path
        title
      }
    }
  }
`