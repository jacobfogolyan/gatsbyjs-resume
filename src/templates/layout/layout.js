/**
 * Layout component that queries for data
 * with Gatsby's StaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/static-query/
 */

import React from 'react'
import PropTypes from 'prop-types'
import { StaticQuery, graphql } from 'gatsby'
import Header from '../header'
import './layout.scss'
import Sidebar from '../../components/sidebar/sidebar';

const Layout = ({ children }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={data => (
      <>
        <Header siteTitle={data.site.siteMetadata.title} />
        <div className="container px-4 py-4">
          <div className="row">
            <main className="col-12 col-md-9 col-xl-8 bd-content" role="main">{children}</main>
            <Sidebar />
          </div>
        </div>
        <footer style={{
          backgroundColor: `#32292F`,
        }}>
        <div className="container px-4">
          <div className="row">
            <div style={{ color: '#F0F7F4'}} className="col-11 py-3 bd-content">
              © {new Date().getFullYear()}, Built by&nbsp;
                Jacob Fogolyan
            </div>
              <div style={{ color: '#F0F7F4' }} className="col-1 py-3">
              linkedin
            </div>
          </div>
        </div>
        </footer>
      </>
    )}
  />
)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
